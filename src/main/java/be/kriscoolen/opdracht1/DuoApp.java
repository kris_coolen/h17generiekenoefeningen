package be.kriscoolen.opdracht1;

public class DuoApp {
    public static void main(String[] args) {
        Duo<String> sd = new Duo<String>("Hello","World");
        Duo<Integer> id = new Duo<Integer>(7,5);


        String s1 = sd.getFirst();
        String s2 = sd.getSecond();
        System.out.println("first: " + sd.getFirst());
        System.out.println("second: " + sd.getSecond());

        sd.swap();
        System.out.println("we hebben hello en world geswapt:");
        System.out.println("first:" + sd.getFirst());
        System.out.println("second:" + sd.getSecond());

        Integer i1 = id.getFirst();
        Integer i2 = id.getSecond();

        System.out.println("Highest string: " + sd.getHighest());
        System.out.println("Highest int: " + id.getHighest());
        System.out.println("Lowest string: " + sd.getLowest());
        System.out.println("Lowest int: " + id.getLowest());

        System.out.println(s1 + " " + s2);
        System.out.println(i1 + i2);

    }
}
