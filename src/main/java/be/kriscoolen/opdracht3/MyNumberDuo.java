package be.kriscoolen.opdracht3;

public class MyNumberDuo<E extends Number & Comparable<E>> extends Duo<E>{

    public MyNumberDuo(E first, E second){
        super(first,second);
    }

    public double getSum(){
        return super.getFirst().doubleValue()+super.getSecond().doubleValue();
    }

}
