package be.kriscoolen.opdracht3;

public class App {

    public static void main(String[] args) {
        Duo<String> duostring1 = new Duo<String>("hello","world");
        StringDuo duostring2 = new StringDuo("hello","world");

        Duo<Integer> duoint1 = new Duo<Integer>(1,2);
        IntegerDuo duoint2 = new IntegerDuo(1,2);

        System.out.println(duoint1.getHighest());
        System.out.println(duoint1.getLowest());

        System.out.println(duoint2.getHighest());
        System.out.println(duoint2.getLowest());

        System.out.println(duostring1.getHighest());
        System.out.println(duostring1.getLowest());

        System.out.println(duostring2.getHighest());
        System.out.println(duostring2.getLowest());

        MyNumberDuo<Double> doubleDuo = new MyNumberDuo<>(2.3,4.7);
        System.out.println(doubleDuo.getSum());


    }



}
