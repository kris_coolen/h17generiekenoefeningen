package be.kriscoolen.opdracht6;

public class App {

    public static void main(String[] args) {
        Duo<String> d1=new Duo("Hello","World");
        Duo<String> d2=new Duo<>("Goodbye","Mars");

        DuoUtility.printDuo(d1);
        DuoUtility.printDuo(d2);
        DuoUtility.swapFirst(d1,d2);
        DuoUtility.PrintUpper(d1);
        DuoUtility.PrintUpper(d2);
    }
}
