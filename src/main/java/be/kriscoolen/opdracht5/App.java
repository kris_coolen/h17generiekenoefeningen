package be.kriscoolen.opdracht5;

public class App {

    public static void main(String[] args) {
        Duo<String> d1=new Duo("Hello","World");
        Duo<Integer> d2=new Duo<>(8,2);

        DuoUtility.printDuo(d1);
        DuoUtility.printDuo(d2);
        DuoUtility.PrintUpper(d1);
        DuoUtility.printSum(d2);
    }
}
