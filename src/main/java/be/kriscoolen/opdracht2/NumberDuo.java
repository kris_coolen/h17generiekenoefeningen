package be.kriscoolen.opdracht2;

public class NumberDuo<E extends Number> {
    private E first;
    private E second;

    public NumberDuo(E first, E second){
        this.first = first;
        this.second = second;
    }

    public E getFirst() {
        return first;
    }

    public void setFirst(E first) {
        this.first = first;
    }

    public E getSecond() {
        return second;
    }

    public void setSecond(E second) {
        this.second = second;
    }

    public void swap(){
        E temp = second;
        second = first;
        first = temp;
    }

    public double getSum(){
        return first.doubleValue()+second.doubleValue();
    }

   /* public E getHighest(){
        if(first.compareTo(second)>=0) return first;
        else return second;
    }

    public E getLowest(){
        if(first.compareTo(second)<=0) return first;
        else return second;
    }*/
}
