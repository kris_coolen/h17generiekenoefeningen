package be.kriscoolen.opdracht4.deel2;

public class App {
    public static void main(String[] args) {
        ComparableDuo<Integer> compDuo = new ComparableDuo(8,3);
        System.out.println("highest: " + compDuo.getHighest());
        System.out.println("lowest: " + compDuo.getLowest());

        ComparableDuo<Float> compDuo2 = new ComparableDuo(2.5f,1.38f);
        System.out.println("highest: " + compDuo2.getHighest());
        System.out.println("lowest: " + compDuo2.getLowest());
    }
}
