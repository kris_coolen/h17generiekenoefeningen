package be.kriscoolen.opdracht4.deel1;

public class ShoePair implements Pair<Shoe> {
    private Shoe leftShoe;
    private Shoe rightShoe;

    public ShoePair(Shoe leftShoe, Shoe rightShoe) {
        this.leftShoe = leftShoe;
        this.rightShoe = rightShoe;
    }

    @Override
    public Shoe getLeft() {
        return leftShoe;
    }

    @Override
    public Shoe getRight() {
        return rightShoe;
    }

    @Override
    public void setLeft(Shoe left) {

        leftShoe = left;

    }

    @Override
    public void setRight(Shoe right) {

        rightShoe = right;

    }

    @Override
    public String toString() {
        return "ShoePair{" +
                "leftShoe=" + leftShoe +
                ", rightShoe=" + rightShoe +
                '}';
    }
}
