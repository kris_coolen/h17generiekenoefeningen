package be.kriscoolen.opdracht4.deel1;

public class GeneralPair<E> implements Pair<E> {
    private E left;
    private E right;

    public GeneralPair(E left, E right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public E getLeft() {
        return left;
    }

    @Override
    public void setLeft(E left) {
        this.left = left;
    }

    @Override
    public E getRight() {
        return right;
    }

    @Override
    public void setRight(E right) {
        this.right = right;
    }

    @Override
    public String toString() {
        return "GeneralPair{" +
                "left=" + left +
                "right=" + right +
                '}';
    }


}

