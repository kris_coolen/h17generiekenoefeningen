package be.kriscoolen.opdracht4.deel1;

public class Shoe {

    int size;
    String brand;
    Side side;

    public Shoe(int size, String brand, Side side) {
        this.size = size;
        this.brand = brand;
        this.side = side;

    }
    public Shoe(Side side){
        this(43,"Van Bommel",side);
    }

    public Shoe(){
        this(40,"Nike",Side.RIGHT);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "Shoe{" +
                "size=" + size +
                ", brand='" + brand + '\'' +
                ", side=" + side +
                '}';
    }
}
