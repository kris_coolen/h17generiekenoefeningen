package be.kriscoolen.opdracht4.deel1;

public class App {
    public static void main(String[] args) {
        Shoe leftShoe = new Shoe(Side.LEFT);
        Shoe rightShoe = new Shoe(Side.RIGHT);
        Pair<Shoe> pair = new ShoePair(leftShoe,rightShoe);
        System.out.println(pair);
        pair.swap();
        System.out.println(pair);

        Shoe leftShoe2 = new Shoe(44,"Ambiorix",Side.LEFT);
        Shoe rightShoe2 = new Shoe(44,"Ambiorix",Side.RIGHT);

        Pair<Shoe> pair2 = new GeneralPair<>(leftShoe2,rightShoe2);
        System.out.println(pair2);
    }



}
